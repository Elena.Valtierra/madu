// CAROUSEL

/* With table (new array) we only have to spacify in the funcion when it is pass the nb of variables in the table or when it is below */

var slide = new Array("assets/images/Banners/banner2Cut.png", "assets/images/Banners/sketch3cut.png", "assets/images/Banners/sketch4cut.png");
var number = 0;


function ChangeSlide(direction) {
    number = number + direction;
    if (number < 0)
        number = slide.length - 1;
    /*(number > slide.length - 1) 
    -> 0 = 3(so that is larger than 2 variables (first image is position 0 then 1, 2, 3..)) -1 (it is never larger than the max position of 3)
    with number = 0 -> tells the position it should take , so it would use the position 0 returning to the initial image */
    if (number > slide.length - 1)
        number = 0;
    document.getElementById("slide").src = slide[number];
}

/* setInterval() va appeler la fonction ChangeSlide avec le paramètre 1 toutes les 4 secondes soit 4000 millisecondes.*/

/*setInterval("ChangeSlide(1)", 4000);*/